//
//  Constants.swift
//  TwitterTrend
//
//  Created by Sahej Kaur on 15/07/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

struct Constants {
  
  struct DeliveryList {
    static let heading = "Things to Deliver"
    static let loadingTitle = "Fetching Deliveries"
  }
  struct DeliveryDetails {
    static let heading = "Delivery Details"
    static let deltaArea = 0.02
  }
  
  struct Image {
    static let kPlaceholderImage = "placeholder"
  }
}


