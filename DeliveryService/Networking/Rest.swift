//
//  Rest.swift
//  TwitterTrend
//
//  Created by Sahej Kaur on 14/07/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

protocol RestInterface {
  func get(path: String, query: String?, headers: [String: String]?, completion:@escaping (Result<Data>) -> Void);
}

open class Rest: NSObject, RestInterface {
  public enum HTTPMethod: String {
    case put = "PUT"
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
  }
  
  public enum APIURLs: String {
    case clientBaseUrl = "https://mock-api-mobile.dev.lalamove.com/"
  }
  
  /// Make a REST connection
  internal func connect(method: String,
                        path: String,
                        query: String? = nil,
                        headers: [String: String]? = nil,
                        jsonObject: JSONObject? = nil,
                        completion: @escaping (Result<Data>) -> Void) throws {
    let apiURL = APIURLs.clientBaseUrl.rawValue
    guard  var components = URLComponents(string: apiURL) else {
      throw ValidationError.invalid("baseURL", apiURL)
    }
    components.path += path
    if let validQuery = query, !validQuery.isEmpty {
      components.query = validQuery
    }
    guard let url = components.url else {
      throw ValidationError.invalid("path", components.path)
    }
    
    
    // Setup Request
    var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
    request.httpMethod = method
    request.allHTTPHeaderFields = headers
    
    // Setup JSON
    if let jsonObject = jsonObject {
      guard JSONSerialization.isValidJSONObject(jsonObject) else {
        print("Invalid JSON Object: \(jsonObject)")
        throw ValidationError.invalid("jsonObject", jsonObject)
      }
      
      let jsonData = try JSONSerialization.data(withJSONObject: jsonObject,
                                                options: [])
      request.setValue("application/json", forHTTPHeaderField: "content-type")
      request.httpBody = jsonData
    }
    let urlSession: URLSession = URLSession.shared
    
    // Make the call
    print("Request :",request)
    urlSession.dataTask(with: request, completionHandler: { data, response, error in
      DispatchQueue.main.async {
        do {
          // Check response errors
          if let responseError = error { throw responseError }
          
          // Successful result
          guard let data = data else { return }
          completion(Result.success(result: data))
          
        } catch {
          if let data = URLCache.shared.cachedResponse(for: request)?.data {
            completion(Result.success(result: data))
          } else {
          print("Rest Error: \(error.localizedDescription)")
          completion(Result.failure(error: error))
          }
        }
      }
      
    }).resume()
    
  }
  
  // MARK: - Public methods
  
  func get(path: String, query: String? = nil, headers: [String: String]? = nil, completion:@escaping (Result<Data>) -> Void) {
    do {
      try connect(method: HTTPMethod.get.rawValue, path: path, query: query,
                  headers: headers, jsonObject: nil, completion: completion)
    } catch {
      completion(Result.failure(error: ReturnError.apiError(code: "", header: "", message: "")))
    }
  }
}
