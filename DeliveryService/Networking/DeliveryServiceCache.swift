//
//  DeliveryServiceCache.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

class DeliveryServiceCache: URLCache {
  private let kExpiryKey = "CACHE-EXPIRE"
  private let kUrlCacheExpireDuration = 60*60*24
  override func cachedResponse(for request: URLRequest) -> CachedURLResponse? {
    
    if let cachedResponse = super.cachedResponse(for: request), let userInfo = cachedResponse.userInfo, let cacheDate = userInfo[kExpiryKey] as? NSDate {
      
        // check if the cache data are expired
        if cacheDate.timeIntervalSinceNow < -Double(kUrlCacheExpireDuration) {
          // remove old cache request
          self.removeCachedResponse(for: request)
        } else {
          // the cache request is still valid
          return cachedResponse
        }
      }
    return nil
  }
  
  override func storeCachedResponse(_ cachedResponse: CachedURLResponse, for request: URLRequest) {
    
    var userInfo = cachedResponse.userInfo ?? [:]
    userInfo[kExpiryKey] = NSDate()
    
    let cachedResponseNew = CachedURLResponse(response: cachedResponse.response, data: cachedResponse.data, userInfo: userInfo, storagePolicy: cachedResponse.storagePolicy)
    
    super.storeCachedResponse(cachedResponseNew, for: request)
  }
}
