//
//  RestUtils.swift
//  TwitterTrend
//
//  Created by Sahej Kaur on 14/07/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

public typealias JSONObject = [String : Any]

public enum ReturnError : Error {
  case apiError(code: String, header: String?, message: String?)
  case invalidJSON
}

public enum Result<T> {
  case success(result: T)
  case failure(error: Error)
}

public enum ValidationError: Error {
  case missing(String)
  case invalid(String, Any)
}

public enum Content<T> {
  case empty
  case success(value: T)
}
