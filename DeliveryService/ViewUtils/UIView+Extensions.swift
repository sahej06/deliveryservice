//
//  UIViewAutoresizingMaskIntoConstraintsForAllSubviews.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

extension UIView {
  public func setAutoresizingMaskIntoConstraintsForAllSubviews() {
    for v in self.subviews {
      v.setAutoresizingMaskIntoConstraintsForAllSubviews()
      v.translatesAutoresizingMaskIntoConstraints = false
    }
  }
  func fullViewConstraints(_ subView: UIView) {
    subView.translatesAutoresizingMaskIntoConstraints = false
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": subView]))
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": subView]))
  }

}
