//
//  UITableViewDequeueExtension.swift
//  TwitterTrend
//
//  Created by Sahej Kaur on 14/07/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation



import UIKit
public extension UITableView {
  
  public func dequeueReusableCustomCellWithIdentifier<T: UITableViewCell>(_ cellIdentifier: String, forIndexPath indexPath: IndexPath) -> T {
    
    guard let cell = dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? T  else {
      fatalError("Error: error with cellIdentifier \(cellIdentifier) for indexPath \(indexPath) is not \(T.self)")
    }
    return cell
  }
}
