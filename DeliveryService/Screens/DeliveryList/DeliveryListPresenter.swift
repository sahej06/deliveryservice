//
//  DeliveryListPresenter.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

protocol DeliveryListPresenterInterface {
  func presentDeliveryList(response: DeliveryListModels.GetDeliveries.Response)
  func presentDeliveryDetails()
  func presentFullPageError(error: Error)
  func presentErrorFetchingNextPage(error: Error)
}

class DeliveryListPresenter: DeliveryListPresenterInterface {
  weak var viewController: DeliveryListViewControllerInterface?
  
  func presentDeliveryList(response: DeliveryListModels.GetDeliveries.Response) {
    let deliveryItems = response.deliveries.map({
      return DeliveryItemDisplay(imageUrl: URL(string: $0.imageUrl), description: "\($0.description) at \($0.location.address)") })
    viewController?.displayDeliveries(viewModel: DeliveryListModels.GetDeliveries.ViewModel(hasNextPage: response.hasNextPage, deliveryItems: deliveryItems))
  }
  
  func presentDeliveryDetails() {
    viewController?.displayDeliveryDetails()
  }
  
  func presentFullPageError(error: Error) {
    viewController?.displayFullPageError()
  }
  
  func presentErrorFetchingNextPage(error: Error) {
    viewController?.displayErrorFetchingNextPage()
  }

}
