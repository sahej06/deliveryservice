//
//  DeliveryListInteractor.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

protocol DeliveryListInteractorInterface {
  var deliveries: [Delivery] { get }
  func fetchDeliveries()
  func didSelectItemAt(index: Int)
  func getSelectedDelivery() -> Delivery
}

class DeliveryListInteractor: DeliveryListInteractorInterface {
  
  private let presenter: DeliveryListPresenterInterface
  private let store: DeliveryStore
  private static let pageSize = 20
  private var offset = 0
  private var selectedDeliveryIndex = 0
  private var isPageFetchInProgress = false
  private var hasNextPage = true
  
  var deliveries: [Delivery] = []
  
  init(presenter: DeliveryListPresenterInterface, store: DeliveryStore) {
    self.presenter = presenter
    self.store = store
  }
  
  func fetchDeliveries() {
    if isPageFetchInProgress || !hasNextPage { return }
    isPageFetchInProgress = true

    
    store.getDeliveryList(request: DeliveryListModels.GetDeliveries.Request(offset: offset, limit: DeliveryListInteractor.pageSize)) { [weak self] (result) -> (Void) in
      
      switch result {
      case .success(result: let deliveries):
        self?.deliveries = (self?.deliveries ?? [Delivery]()) + deliveries
        self?.offset = self?.deliveries.count ?? (self?.offset ?? 0)
        self?.hasNextPage = deliveries.count < DeliveryListInteractor.pageSize ? false : true
        self?.presenter.presentDeliveryList(response: DeliveryListModels.GetDeliveries.Response(deliveries: self?.deliveries ?? [], hasNextPage: self?.hasNextPage ?? false))
      case .failure(error: let error):
        if let deliveries = self?.deliveries, deliveries.isEmpty {
          self?.presenter.presentFullPageError(error: error)
        } else {
          self?.presenter.presentErrorFetchingNextPage(error: error)
        }
        print("error fetching deliveries : " + error.localizedDescription)
      }
      
      self?.isPageFetchInProgress = false
    }
  }

  func didSelectItemAt(index: Int) {
    selectedDeliveryIndex = index
    presenter.presentDeliveryDetails()
  }
  
  func getSelectedDelivery() -> Delivery {
    return deliveries[selectedDeliveryIndex]
  }

}
