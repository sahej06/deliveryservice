//
//  ViewController.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

protocol DeliveryListViewControllerInterface: class {
  func displayDeliveries(viewModel: DeliveryListModels.GetDeliveries.ViewModel)
  func displayDeliveryDetails()
  func displayFullPageError()
  func displayErrorFetchingNextPage()
}

class DeliveryListViewController: UIViewController {
  var deliveryItems: [DeliveryItemDisplay] = [] {
    didSet {
      tableView.reloadData()
    }
  }
  var interactor: DeliveryListInteractorInterface?
  var router: DeliveryListRouterInterface?
  
  var tableView = UITableView()
  var errorView = FullPageErrorView()
  var loaderView = FullPageLoaderView()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupHeader()
    setupTableView()
    setupFullPageErrorView()
    setupLoaderView()
    fetchDeliveries()
  }
  
  func setupHeader() {
    self.navigationItem.title = Constants.DeliveryList.heading
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
  
  func setupTableView() {
    tableView = UITableView()
    view.addSubview(tableView)
    view.fullViewConstraints(tableView)
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(DeliveryItemCell.self, forCellReuseIdentifier: DeliveryItemCell.kIdentifier)
    tableView.separatorStyle = .none
    tableView.backgroundColor = UIColor.lightGray
  }
  
  func setupFooter(_ hasNextPage: Bool) {
    if hasNextPage {
      let view = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
      view.color = .black
      view.startAnimating()
      tableView.tableFooterView = view
    } else {
      tableView.tableFooterView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: 8))
    }
  }
  
  func setupFullPageErrorView() {
    errorView.delegate = self
    errorView.backgroundColor = .white
    view.addSubview(errorView)
    view.fullViewConstraints(errorView)
  }
  
  func setupLoaderView() {
    view.addSubview(loaderView)
    view.fullViewConstraints(loaderView)
  }
  
  @objc func fetchNextPageDetails() {
    setupFooter(true)
    interactor?.fetchDeliveries()
  }
  
  func fetchDeliveries() {
    loaderView.isHidden = false
    tableView.isHidden = true
    errorView.isHidden = true
    interactor?.fetchDeliveries()
  }
}

extension DeliveryListViewController: DeliveryListViewControllerInterface {
  
  func displayDeliveries(viewModel: DeliveryListModels.GetDeliveries.ViewModel) {
    loaderView.isHidden = true
    errorView.isHidden = true
    tableView.isHidden = false
    deliveryItems = viewModel.deliveryItems
    setupFooter(viewModel.hasNextPage)
  }
  
  func displayDeliveryDetails() {
    router?.navigateToDeliveryDetail()
  }
  
  func displayFullPageError() {
    loaderView.isHidden = true
    errorView.isHidden = false
    tableView.isHidden = false
  }
  
  func displayErrorFetchingNextPage() {
    let view = NextPageErrorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
    view.retryButton?.addTarget(self, action: #selector(fetchNextPageDetails), for: .touchUpInside)
    tableView.tableFooterView = view
  }
  
}

extension DeliveryListViewController: RetryDelegate {
  func retryTapped() {
    fetchDeliveries()
  }
}
