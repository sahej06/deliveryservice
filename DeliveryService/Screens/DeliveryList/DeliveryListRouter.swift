//
//  DeliveryListRouter.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

protocol DeliveryListRouterInterface {
  func navigateToDeliveryDetail()
}

class DeliveryListRouter: DeliveryListRouterInterface {
  weak var viewController: DeliveryListViewController?
  
  func navigateToDeliveryDetail() {
    guard let delivery = viewController?.interactor?.getSelectedDelivery() else { return }
    let detailViewController = DeliveryDetailBuilder.createDeliveryDetail(delivery: delivery)
    viewController?.navigationController?.pushViewController(detailViewController, animated: false)
  }
}
