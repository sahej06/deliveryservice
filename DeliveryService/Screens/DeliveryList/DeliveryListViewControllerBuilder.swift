//
//  DeliveryListViewControllerBuilder.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

class DeliveryListBuilder {
  class func createDeliveryList() -> UIViewController {
    let viewController = DeliveryListViewController()
    
    let router = DeliveryListRouter()
    router.viewController = viewController
    
    viewController.router = router
    
    let presenter = DeliveryListPresenter()
    presenter.viewController = viewController
    
    let interactor = DeliveryListInteractor(presenter: presenter, store: DeliveryRestStore(rest: Rest()))

    viewController.interactor = interactor
    return viewController
  }
}
