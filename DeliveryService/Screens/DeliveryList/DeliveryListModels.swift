//
//  DeliveryListModels.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

struct DeliveryListModels {
  struct GetDeliveries {
    struct Request {
      let offset: Int
      let limit: Int
    }
    
    struct Response {
      let deliveries: [Delivery]
      let hasNextPage: Bool
    }
    
    struct ViewModel {
      let hasNextPage: Bool
      let deliveryItems: [DeliveryItemDisplay]
    }
  }
}

struct DeliveryItemDisplay {
  let imageUrl: URL?
  let description: String
}


extension DeliveryListModels.GetDeliveries.Request {
  func toQueryString() -> String {
    return "offset=\(offset)&limit=\(limit)"
  }
}

