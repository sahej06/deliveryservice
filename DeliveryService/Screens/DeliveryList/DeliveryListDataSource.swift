//
//  DeliveryListDataSource.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

extension DeliveryListViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return deliveryItems.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: DeliveryItemCell =  tableView.dequeueReusableCustomCellWithIdentifier(DeliveryItemCell.kIdentifier, forIndexPath: indexPath)
    cell.updateView(data: deliveryItems[indexPath.row])
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    interactor?.didSelectItemAt(index: indexPath.row)
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if indexPath.row == deliveryItems.count - 1 {
      interactor?.fetchDeliveries()
    }
  }
  
}
