//
//  DeliveryDetailsModels.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation
import MapKit

struct DeliveryDetailsModels {
  struct Request {  }
  
  struct Response {
    let delivery: Delivery
  }
  
  struct ViewModel {
    let deliveryDescription: String
    let location: CLLocation
    let imageUrl: URL?
  }
}
