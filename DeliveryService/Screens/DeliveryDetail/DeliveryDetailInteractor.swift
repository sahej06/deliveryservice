//
//  DeliveryDetailInteractor.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

protocol DeliveryDetailInteractorInterface {
  func fetchDetails()
}

class DeliveryDetailInteractor: DeliveryDetailInteractorInterface {
  let delivery: Delivery
  let presenter: DeliveryDetailPresenterInterface
  
  init(delivery: Delivery, presenter: DeliveryDetailPresenterInterface) {
    self.delivery = delivery
    self.presenter = presenter
  }
  
  func fetchDetails() {
    presenter.presentDetails(response: DeliveryDetailsModels.Response(delivery: delivery))
  }
}
