//
//  DeliveryDetailViewController.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit
import MapKit

protocol DeliveryDetailViewControllerInterface: class {
  func displayDetails(viewModel: DeliveryDetailsModels.ViewModel)
}

class DeliveryDetailViewController: UIViewController {
  var interactor: DeliveryDetailInteractorInterface?
  
  private var mapView: MKMapView?
  private var detailView: DeliveryDetailView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = Constants.DeliveryDetails.heading
    view.backgroundColor = .white
    setupMapView()
    setupDetailView()
    interactor?.fetchDetails()
  }
  
  func setupMapView() {
    mapView = MKMapView()
    guard let mapView = mapView else { return }
    view.addSubview(mapView)
    view.fullViewConstraints(mapView)
  }
  
  func setupDetailView() {
    detailView = DeliveryDetailView()
    guard let detailView = detailView else { return }
    view.addSubview(detailView)
    detailView.backgroundColor = UIColor(white: 1, alpha: 0.8)
    detailView.translatesAutoresizingMaskIntoConstraints = false
    detailView.layer.borderWidth = 0.5
    detailView.layer.borderColor = UIColor.gray.cgColor
    detailView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
    detailView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
    detailView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
    detailView.initializeViews()
  }
  
}

extension DeliveryDetailViewController: DeliveryDetailViewControllerInterface {
  
  func displayDetails(viewModel: DeliveryDetailsModels.ViewModel) {
    
    let center = CLLocationCoordinate2D(latitude: viewModel.location.coordinate.latitude, longitude: viewModel.location.coordinate.longitude)
    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: Constants.DeliveryDetails.deltaArea, longitudeDelta: Constants.DeliveryDetails.deltaArea))
    mapView?.setRegion(region, animated: true)
    let annotation = MKPointAnnotation()
    annotation.coordinate = center
    mapView?.addAnnotation(annotation)

    detailView?.descriptionLabel?.text = viewModel.deliveryDescription
    detailView?.deliveryImageView?.kf.setImage(with: viewModel.imageUrl, placeholder: UIImage(named: Constants.Image.kPlaceholderImage))
  }
  
}
