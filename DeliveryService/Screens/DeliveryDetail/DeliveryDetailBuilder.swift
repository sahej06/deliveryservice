//
//  DeliveryDetailBuilder.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

class DeliveryDetailBuilder {
  class func createDeliveryDetail(delivery: Delivery) -> UIViewController {
    let viewController = DeliveryDetailViewController()
    
    let presenter = DeliveryDetailPresenter()
    presenter.viewController = viewController
    
    let interactor = DeliveryDetailInteractor(delivery: delivery, presenter: presenter)
    viewController.interactor = interactor
    
    return viewController
  }
}
