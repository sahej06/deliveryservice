//
//  DeliveryDetailPresenter.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 18/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation
import MapKit
protocol DeliveryDetailPresenterInterface {
  func presentDetails(response: DeliveryDetailsModels.Response)
}

class DeliveryDetailPresenter: DeliveryDetailPresenterInterface {
  weak var viewController: DeliveryDetailViewControllerInterface?
  
  func presentDetails(response: DeliveryDetailsModels.Response) {
    
    let viewModel = DeliveryDetailsModels.ViewModel(deliveryDescription: "\(response.delivery.description) at \(response.delivery.location.address)", location: CLLocation(latitude: response.delivery.location.lat, longitude: response.delivery.location.lng), imageUrl: URL(string: response.delivery.imageUrl))
    
    viewController?.displayDetails(viewModel: viewModel)
  }
}
