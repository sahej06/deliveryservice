//
//  DeliveryRestStore.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//
import Foundation

protocol DeliveryStore {
  func getDeliveryList(request: DeliveryListModels.GetDeliveries.Request, completion: @escaping (Result<[Delivery]>) -> (Void))
}


class DeliveryRestStore: DeliveryStore {
  
  let restClient: RestInterface
  
  enum Endpoint: String {
    case deliveries = "deliveries"
  }
  
  init(rest: RestInterface) {
    restClient = rest
  }
  
  func getDeliveryList(request: DeliveryListModels.GetDeliveries.Request, completion: @escaping (Result<[Delivery]>) -> (Void)) {
    restClient.get(path: Endpoint.deliveries.rawValue, query: request.toQueryString(), headers: [:]) { (dataResult) in
      switch dataResult {
      case .success(let data):
        do {
          let deliveries = try JSONDecoder().decode([Delivery].self, from: data)
          completion(Result.success(result: deliveries))
        } catch {
          completion(Result.failure(error: ReturnError.invalidJSON))
        }
      case .failure(let error):
        completion(Result.failure(error: error))
      }
    }
  }
  
}
