//
//  DeliveryList.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

struct Delivery: Codable {
  let id: Int
  let description: String
  let imageUrl: String
  let location: DeliveryLocation
}
