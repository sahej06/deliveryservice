//
//  DeliveryLocation.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import Foundation

struct DeliveryLocation: Codable {
  let lat: Double
  let lng: Double
  let address: String
}
