//
//  FullPageLoaderView.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 20/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

class FullPageLoaderView: UIView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initializeViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initializeViews()
  }
  
  func initializeViews() {
    backgroundColor = .white
    
    let label = UILabel()
    label.text = Constants.DeliveryList.loadingTitle
    label.translatesAutoresizingMaskIntoConstraints = false
    addSubview(label)
    label.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -22).isActive = true
    label.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    
    let view = UIActivityIndicatorView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.color = .black
    view.startAnimating()
    addSubview(view)
    view.heightAnchor.constraint(equalToConstant: 44).isActive = true
    view.widthAnchor.constraint(equalToConstant: 44).isActive = true
    view.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 0).isActive = true
    view.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
  }
}
