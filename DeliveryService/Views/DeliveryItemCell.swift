//
//  DeliveryItemCell.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 17/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit
import Kingfisher

class DeliveryItemCell: UITableViewCell {
  var cellContent: DeliveryDetailView?
  
  static let kIdentifier = "DeliveryItem"
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.initializeViews()
  }
  
  override func prepareForReuse() {
   cellContent?.deliveryImageView?.image = UIImage(named: Constants.Image.kPlaceholderImage)
  }
  
  func updateView(data: DeliveryItemDisplay) {
    cellContent?.descriptionLabel?.text = data.description
    cellContent?.deliveryImageView?.kf.setImage(with: data.imageUrl, placeholder: UIImage(named: Constants.Image.kPlaceholderImage))
  }
  
  func initializeViews() {
    selectionStyle = .none
    cellContent = DeliveryDetailView()
    guard let cellContent = cellContent else { return }
    
    contentView.addSubview(cellContent)
    contentView.backgroundColor = .lightGray
    cellContent.backgroundColor = .white
    cellContent.translatesAutoresizingMaskIntoConstraints = false
    cellContent.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
    cellContent.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
    cellContent.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
    cellContent.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
  }
}
