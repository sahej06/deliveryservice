//
//  FullPageErrorView.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 19/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

protocol RetryDelegate: class {
  func retryTapped()
}

class FullPageErrorView: UIView {
  private var label: UILabel?
  private var retryButton: UIButton?
  weak var delegate: RetryDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initializeViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initializeViews()
  }
  
  func initializeViews() {
    label = UILabel()
    retryButton = UIButton()
    
    guard let label = label, let retryButton = retryButton else { return }
    
    label.text = "Error Fetching Deliveries.\n Please Tap Retry Button."
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
    addSubview(label)
    label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    label.leadingAnchor.constraint(greaterThanOrEqualTo: self.leadingAnchor, constant: 16).isActive = true
    
    retryButton.setTitle("Retry", for: .normal)
    retryButton.setTitleColor(.black, for: .normal)
    retryButton.layer.borderColor = UIColor.black.cgColor
    retryButton.layer.borderWidth = 2
    retryButton.translatesAutoresizingMaskIntoConstraints = false
    addSubview(retryButton)
    retryButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    retryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 16).isActive = true
    retryButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
    retryButton.addTarget(self, action: #selector(retryTapped), for: .touchUpInside)
  }
  
  @objc private func retryTapped() {
    delegate?.retryTapped()
  }
}
