//
//  NextPageErrorView.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 20/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import UIKit

class NextPageErrorView: UIView {
  var retryButton: UIButton?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initializeViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initializeViews()
  }

  func initializeViews() {
    retryButton = UIButton()
    guard let retryButton = retryButton else { return }
    addSubview(retryButton)
    retryButton.setTitleColor(.black, for: .normal)
    retryButton.setTitle("Error fetching. Tap to Retry. >", for: .normal)
    retryButton.translatesAutoresizingMaskIntoConstraints = false
    retryButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
    retryButton.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
    retryButton.widthAnchor.constraint(equalToConstant: 300).isActive = true
  }
}
