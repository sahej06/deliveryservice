//
//  DeliveryDetailView.swift
//  DeliveryService
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//
import UIKit
import Kingfisher

class DeliveryDetailView: UIView {
  var deliveryImageView: UIImageView?
  var descriptionLabel: UILabel?
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initializeViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initializeViews()
  }
  
  func initializeViews() {
    deliveryImageView = UIImageView()
    descriptionLabel = UILabel()
    
    guard let deliveryImageView = deliveryImageView, let descriptionLabel = descriptionLabel else { return }
    
    addSubview(deliveryImageView)
    deliveryImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
    deliveryImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
    deliveryImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    deliveryImageView.heightAnchor.constraint(equalToConstant: 75).isActive = true
    deliveryImageView.widthAnchor.constraint(equalToConstant: 75).isActive = true
    
    addSubview(descriptionLabel)
    descriptionLabel.leadingAnchor.constraint(equalTo: deliveryImageView.trailingAnchor, constant: 12).isActive = true
    descriptionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
    descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
    descriptionLabel.numberOfLines = 0
    setAutoresizingMaskIntoConstraintsForAllSubviews()
  }
}
