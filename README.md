- Used Xcode Version 10.0

- Used Clean Swift Architecture. For reference :
https://clean-swift.com/clean-swift-ios-architecture/
(also known as VIP pattern, inspired by Uncle Bob's clean Architecture)

- Unit tests Covered

- UI tests For Controller not covered, many approaches - EarlGrey, XCUITest, KIF

- Screenshots attached for reference
    - Code Coverage Screenshot
    - App Screenshots for various scenarios

- Libraries used : KingFisher for image caching
    - Added through PodFile. The Pods are checked in. Although practice is to add them in .gitignore. Wanted to remove pod install dependency while running the app
    
- Data Caching: Done creating a wrapper over URLCache with 24 hr expiry

