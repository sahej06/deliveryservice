//
//  DeliveryRestStore.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
@testable import DeliveryService

class DeliveryRestStoreSpec: XCTestCase {
  var sut: DeliveryRestStore?
  var restSpy: RestSpy!
  override func setUp() {
    restSpy = RestSpy()
    sut = DeliveryRestStore(rest: restSpy)
  }
  
  func testGetDeliveriesSuccess() {
    //given
    let req = DeliveryListModels.GetDeliveries.Request(offset: 0, limit: 20)
    var successCalled = false
    var deliveriesOutput = [Delivery]()
    do {
      let jsonArray = [["id": 1,
                      "description": "delivery1",
                      "imageUrl": "some-url",
                      "location": ["lat": 23.4,
                                   "lng": 32.4,
                                   "address":"address1"]]] as [[String : Any]]
      restSpy?.mockData = try JSONSerialization.data(withJSONObject: jsonArray, options: .prettyPrinted)
      
    } catch {
      
    }
    //when
    sut?.getDeliveryList(request: req, completion: { (result) -> (Void) in
      switch result {
      case .success(result: let deliveries):
        successCalled = true
        deliveriesOutput = deliveries
      default: break
      }
    })
    //then
    XCTAssertTrue(successCalled)
    XCTAssertEqual(deliveriesOutput.count,1)
    XCTAssertEqual(deliveriesOutput.first?.id,1)
    XCTAssertEqual(deliveriesOutput.first?.description,"delivery1")
    XCTAssertEqual(deliveriesOutput.first?.imageUrl,"some-url")
    XCTAssertEqual(deliveriesOutput.first?.location.address,"address1")
    XCTAssertEqual(deliveriesOutput.first?.location.lat,23.4)
    XCTAssertEqual(deliveriesOutput.first?.location.lng,32.4)
    
  }
  
  func testGetDeliveriesFail() {
    //given
    let req = DeliveryListModels.GetDeliveries.Request(offset: 0, limit: 20)
    var failCalled = false
    restSpy.failRest = true

    //when
    sut?.getDeliveryList(request: req, completion: { (result) -> (Void) in
      switch result {
      case .failure(error: _):
        failCalled = true
      default: break
      }
    })
    //then
    XCTAssertTrue(failCalled)
  }

}

class RestSpy: RestInterface {
  var failRest = false
  var mockData: Data?
  func get(path: String, query: String?, headers: [String : String]?, completion: @escaping (Result<Data>) -> Void) {
    if !failRest, let data = mockData {
      completion(Result.success(result:data))
    } else {
      completion(Result.failure(error: ReturnError.invalidJSON))
    }
  }
}
