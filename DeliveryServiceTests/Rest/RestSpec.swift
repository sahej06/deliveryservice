//
//  RestSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 22/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//
import XCTest
@testable import DeliveryService

class RestTests: XCTestCase {
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  
  // MARK: - Tests
  
  func testEnumHTTPMethodPUTRawValue() {
    // Given
    let method = Rest.HTTPMethod.put
    
    // When
    
    // Then
    XCTAssertEqual(method.rawValue, "PUT")
  }
  
  func testEnumHTTPMethodGETRawValue() {
    // Given
    let method = Rest.HTTPMethod.get
    
    // When
    
    // Then
    XCTAssertEqual(method.rawValue, "GET")
  }
  
  func testEnumHTTPMethodPOSTRawValue() {
    // Given
    let method = Rest.HTTPMethod.post
    
    // When
    
    // Then
    XCTAssertEqual(method.rawValue, "POST")
  }
  
  func testEnumHTTPMethodDELETERawValue() {
    // Given
    let method = Rest.HTTPMethod.delete
    
    // When
    
    // Then
    XCTAssertEqual(method.rawValue, "DELETE")
  }
}
