//
//  DeliveryDetailViewControllerSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 22/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
@testable import DeliveryService

class DeliveryDetailViewControllerSpec: XCTestCase {
  var sut: DeliveryDetailViewController?
  var interactor: DeliveryDetailInteractorSpy!
  
  override func setUp() {
    sut = DeliveryDetailViewController()
    interactor = DeliveryDetailInteractorSpy()
    sut?.interactor = interactor
  }
  
  func testFetchDetail() {
    sut?.viewDidLoad()
    XCTAssertTrue(interactor.fetchDetailsCalled)
  }
  
}

class DeliveryDetailInteractorSpy: DeliveryDetailInteractorInterface {
  var fetchDetailsCalled = false
  func fetchDetails() {
    fetchDetailsCalled = true
  }
}
