//
//  DeliveryDetailInteractorSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
@testable import DeliveryService

class DeliveryDetailInteractorSpec: XCTestCase {
  var sut: DeliveryDetailInteractor?
  var presenterSpy: DeliveryDetailPresenterSpy!
  
  override func setUp() {
    let delivery = Delivery(id: 1, description: "delivery1", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
    presenterSpy = DeliveryDetailPresenterSpy()
    
    sut = DeliveryDetailInteractor(delivery: delivery, presenter: presenterSpy)
  }
  
  func testFetchDetails() {
    //when
    sut?.fetchDetails()
    //then
    XCTAssertTrue(presenterSpy.presentDetailsCalled)
    XCTAssertEqual(presenterSpy.response?.delivery.description, "delivery1")
    XCTAssertEqual(presenterSpy.response?.delivery.location.address, "address1")

  }
  
}

class DeliveryDetailPresenterSpy: DeliveryDetailPresenterInterface {
  var presentDetailsCalled = false
  var response: DeliveryDetailsModels.Response?
  func presentDetails(response: DeliveryDetailsModels.Response) {
    presentDetailsCalled = true
    self.response = response
  }
}
