//
//  DeliveryDetailPresenterSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
@testable import DeliveryService

class DeliveryDetailPresenterSpec: XCTestCase {
  var sut: DeliveryDetailPresenter?
  var viewControllerSpy: DeliveryDetailViewControllerSpy!
  
  override func setUp() {
    sut = DeliveryDetailPresenter()
    viewControllerSpy = DeliveryDetailViewControllerSpy()
    sut?.viewController = viewControllerSpy
  }
  
  func testPresentDetails() {
    //given
    let delivery = Delivery(id: 1, description: "delivery1", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
    let response = DeliveryDetailsModels.Response(delivery: delivery)
    
    //when
    sut?.presentDetails(response: response)
    
    //then
    XCTAssertTrue(viewControllerSpy.displayDetailsCalled)
    XCTAssertEqual(viewControllerSpy?.viewModel?.deliveryDescription, "delivery1 at address1")
    XCTAssertEqual(viewControllerSpy?.viewModel?.location.coordinate.latitude, 23.4)
    XCTAssertEqual(viewControllerSpy?.viewModel?.location.coordinate.longitude, 32.4)
  }
}

class DeliveryDetailViewControllerSpy: DeliveryDetailViewControllerInterface {
  var displayDetailsCalled = false
  var viewModel: DeliveryDetailsModels.ViewModel?
  
  func displayDetails(viewModel: DeliveryDetailsModels.ViewModel) {
    displayDetailsCalled = true
    self.viewModel = viewModel
  }
}
