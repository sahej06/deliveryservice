//
//  DeliveryListInteractorSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
@testable import DeliveryService

class DeliveryListInteractorSpec: XCTestCase {
  
  var sut: DeliveryListInteractor?
  var presenterSpy: DeliveryListPresenterSpy!
  var store: DeliveryStoreSpy!
  
  override func setUp() {
    presenterSpy = DeliveryListPresenterSpy()
    store = DeliveryStoreSpy()
    sut = DeliveryListInteractor(presenter: presenterSpy, store: store)
  }
  
  func testFetchDeliveryListSuccess() {

    //when
    sut?.fetchDeliveries()

    //then
    XCTAssertTrue(presenterSpy.presentDeliveryListCalled)
    XCTAssertEqual(presenterSpy.response?.deliveries.count, 1)
    XCTAssertEqual(presenterSpy.response?.deliveries.first?.id, 1)
    XCTAssertEqual(presenterSpy.response?.deliveries.first?.description,"delivery1")

  }
  
  func testFetchDeliveryListFailure() {

    //given
    store.failGetDeliveryListCall = true

    //when
    sut?.fetchDeliveries()

    //then
    XCTAssertTrue(presenterSpy.presentFullPageErrorCalled)
  }
  
  func testFetchNextPageFailure() {
    
    //given
    store.failGetDeliveryListCall = true
    let delivery = Delivery(id: 1, description: "delivery1", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
    sut?.deliveries = [delivery]
    
    //when
    sut?.fetchDeliveries()
    
    //then
    XCTAssertTrue(presenterSpy.presentErrorFetchingNextPageCalled)
  }

  func testDeliveryDetail() {
    //given
    let delivery1 = Delivery(id: 1, description: "delivery1", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
    let delivery2 = Delivery(id: 1, description: "delivery2", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address2"))

    sut?.deliveries = [delivery1, delivery2]
    
    //when
    sut?.didSelectItemAt(index: 1)
    
    //then
    XCTAssertEqual(sut?.getSelectedDelivery().description, "delivery2")
    XCTAssertEqual(sut?.getSelectedDelivery().location.address, "address2")
    XCTAssertTrue(presenterSpy.presentDeliveryDetailsCalled)
  }
  
}


class DeliveryListPresenterSpy: DeliveryListPresenterInterface {
  var presentDeliveryListCalled = false
  var presentDeliveryDetailsCalled = false
  var presentFullPageErrorCalled = false
  var presentErrorFetchingNextPageCalled = false
  var response: DeliveryListModels.GetDeliveries.Response?
  
  func presentDeliveryList(response: DeliveryListModels.GetDeliveries.Response) {
    presentDeliveryListCalled = true
    self.response = response
  }
  
  func presentDeliveryDetails() {
    presentDeliveryDetailsCalled = true
  }
  
  func presentFullPageError(error: Error) {
    presentFullPageErrorCalled = true
  }
  
  func presentErrorFetchingNextPage(error: Error) {
    presentErrorFetchingNextPageCalled = true
  }
}

class DeliveryStoreSpy: DeliveryStore {
  var failGetDeliveryListCall = false
  func getDeliveryList(request: DeliveryListModels.GetDeliveries.Request, completion: @escaping (Result<[Delivery]>) -> (Void)) {
    
    if !failGetDeliveryListCall {
      
      let delivery = Delivery(id: 1, description: "delivery1", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
      completion(Result.success(result: [delivery]))
    
    } else {
      completion(Result.failure(error: ReturnError.invalidJSON))
    }
  }
  
}
