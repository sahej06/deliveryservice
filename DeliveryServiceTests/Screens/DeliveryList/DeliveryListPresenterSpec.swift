//
//  DeliveryListPresenterSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 21/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
@testable import DeliveryService

class DeliveryListPresenterSpec: XCTestCase {
  var sut: DeliveryListPresenter?
  var viewController: DeliveryListViewControllerSpy!
  
  override func setUp() {
    sut = DeliveryListPresenter()
    viewController = DeliveryListViewControllerSpy()
    sut?.viewController = viewController
  }
  
  func testPresentDeliveryList() {
    //given
    let delivery = Delivery(id: 1, description: "delivery1", imageUrl: "url1", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
    let response = DeliveryListModels.GetDeliveries.Response(deliveries: [delivery], hasNextPage: true)
    
    //when
    sut?.presentDeliveryList(response: response)
    
    //then
    XCTAssertTrue(viewController.displayDeliveriesCalled)
    XCTAssertEqual(viewController.viewModel?.deliveryItems.count, 1)
    XCTAssertEqual(viewController.viewModel?.deliveryItems.first?.description, "delivery1 at address1")
  }
  
  func testPresentDeliveryDetails() {
    //when
    sut?.presentDeliveryDetails()    
    //then
    XCTAssertTrue(viewController.displayDeliveryDetailsCalled)
  }

  func testPresentFullPageError() {
    //when
    sut?.presentFullPageError(error: ReturnError.invalidJSON)
    //then
    XCTAssertTrue(viewController.displayFullPageErrorCalled)
  }
  
  func testPresentErrorFetchingNextPage() {
    //when
    sut?.presentErrorFetchingNextPage(error: ReturnError.invalidJSON)
    //then
    XCTAssertTrue(viewController.displayErrorFetchingNextPageCalled)
  }

  
}

class DeliveryListViewControllerSpy: DeliveryListViewControllerInterface {
  var displayDeliveriesCalled = false
  var displayDeliveryDetailsCalled = false
  var displayFullPageErrorCalled = false
  var displayErrorFetchingNextPageCalled = false
  var viewModel: DeliveryListModels.GetDeliveries.ViewModel?
  
  
  func displayDeliveries(viewModel: DeliveryListModels.GetDeliveries.ViewModel) {
    self.viewModel = viewModel
    displayDeliveriesCalled = true
  }
  
  func displayDeliveryDetails() {
    displayDeliveryDetailsCalled = true
  }
  
  func displayFullPageError() {
    displayFullPageErrorCalled = true
  }
  
  func displayErrorFetchingNextPage() {
    displayErrorFetchingNextPageCalled = true
  }
}
