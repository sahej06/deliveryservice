//
//  DeliveryListViewControllerSpec.swift
//  DeliveryServiceTests
//
//  Created by Sahej Kaur on 22/09/18.
//  Copyright © 2018 Sahej. All rights reserved.
//

import XCTest
import UIKit
@testable import DeliveryService

class DeliveryListViewControllerSpec: XCTestCase {
  var sut: DeliveryListViewController?
  var interactorSpy: DeliveryListInteractorSpy!
  var routerSpy: DeliveryListRouterSpy!
  override func setUp() {
    sut = DeliveryListViewController()
    interactorSpy = DeliveryListInteractorSpy()
    routerSpy = DeliveryListRouterSpy()
    sut?.interactor = interactorSpy
    sut?.router = routerSpy
  }
  
  func testFetchDeliveries() {
    //when
    sut?.viewDidLoad()
    //then
    XCTAssertTrue(interactorSpy.fetchDeliveriesCalled)
  }
  
  func testFetchDeliveriesOnRetryTapped() {
    //when
    sut?.retryTapped()
    //then
    XCTAssertTrue(interactorSpy.fetchDeliveriesCalled)
  }
  
  func testFetchDeliveriesOnNextPage() {
    //when
    sut?.fetchNextPageDetails()
    //then
    XCTAssertTrue(interactorSpy.fetchDeliveriesCalled)
  }

  func testNavigateToDeliveryDetail() {
    //when
    sut?.displayDeliveryDetails()
    //then
    XCTAssertTrue(routerSpy.navigateToDeliveryDetailCalled)
  }
  
  func testDidSelectItem() {
    //when
    sut?.tableView(sut!.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
    //then
    XCTAssertTrue(interactorSpy.didSelectItemCalled)
  }

}
class DeliveryListInteractorSpy: DeliveryListInteractorInterface {
  
  var delivery1 = Delivery(id: 1, description: "delivery1", imageUrl: "", location: DeliveryLocation(lat: 23.4, lng: 32.4, address: "address1"))
  var deliveries: [Delivery] = []
  var fetchDeliveriesCalled = false
  var didSelectItemCalled = false
  
  func fetchDeliveries() {
    fetchDeliveriesCalled = true
  }
  
  func didSelectItemAt(index: Int) {
    didSelectItemCalled = true
  }
  
  func getSelectedDelivery() -> Delivery {
    return delivery1
  }
}

class DeliveryListRouterSpy: DeliveryListRouterInterface {
  var navigateToDeliveryDetailCalled = false
  
  func navigateToDeliveryDetail() {
    navigateToDeliveryDetailCalled = true
  }
  
}
